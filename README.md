El Sistema de Monitoreo de Diaphorina Citri (SIMDIA), es el resultado de un esfuerzo conjunto de todas las partes involucradas en la sanidad vegetal de los cítricos.

El sistema fue concebido para ser una herramienta más de apoyo en la toma de decisiones de productores, técnicos y autoridades del Pais. El SIMDIA cuenta con diferentes componentes que lo hacen útil y versátil en el manejo de los datos, además de que se encuentra en constante renovación al incluir la componente científica como parte de su desarrollo. 

En esta primera etapa el SIMDIA está encaminado al análisis de los datos que toman los organismos auxiliares del vector Diaphorina Citri."