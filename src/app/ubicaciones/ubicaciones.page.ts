import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { UbicacionesService } from '../services/ubicaciones.service';
import { LoadingController } from '@ionic/angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-ubicaciones',
  templateUrl: './ubicaciones.page.html',
  styleUrls: ['./ubicaciones.page.scss'],
})
export class UbicacionesPage implements OnInit {
  latitud:number;
  longitud:number;
  ubicaciones: any = [];
  loading:any;

  constructor(
    public sqlite: SQLite, 
    public ubicacion: UbicacionesService, 
    public loadingController: LoadingController,
    private launchNavigator: LaunchNavigator,
    private geolocation: Geolocation,

    ){ 
    this.getUbicaciones();
    this.location();
   }

  ngOnInit() {
  }
  getUbicaciones(){
    this.cargandoMessage('Cargando');
    this.ubicacion.getTrampas()
    .then(res =>{
       setTimeout(()=>{
          this.ubicaciones = res;
          this.loading.dismiss();
      },1500);
      //this.ubicaciones = res;
    }).catch(error =>{
      alert("Error! "+error);
    })
  }
  location()
  {
      this.geolocation.getCurrentPosition()
      .then((resp) => {
          setTimeout(()=>{
              this.loading.dismiss();
              this.latitud = resp.coords.latitude;
              this.longitud = resp.coords.longitude;
          },1500);
      }).catch((error) => {
        
      }); 
  }
  async cargandoMessage(text:string){
    this.loading = await this.loadingController.create({
      message: text
    });
    return this.loading.present();
  }
  async navigate(x:any, y:any){
    let destination = [y, x];
    let options: LaunchNavigatorOptions ={
      start: [this.latitud, this.longitud],
      app: this.launchNavigator.APP.GOOGLE_MAPS,
    }
    this.launchNavigator.navigate(destination,options)
        .then(res =>{
            alert('Success');
        }).catch(error =>{
            alert("Error! "+error);
        })
  }
}
