import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'calculateDistanceUpdate'
})
export class CalculateDistanceUpdatePipe implements PipeTransform {

 transform(value: any, latitud:number, longitud:number, y:number, x:number ): any {
      //y: latitud, x: longitud
    let total:number;
    let p = 0.017453292519943295;
    let c = Math.cos;
    let a = 0.5 - c((latitud-y) * p) / 2 + c(y * p) *c((latitud) * p) * (1 - c(((longitud- x) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a)));
    let mt = dis * 1000;
    total = Math.trunc(mt);
    
    return total;
    
    
  }

}
