import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrotesDetallePage } from './brotes-detalle.page';

describe('BrotesDetallePage', () => {
  let component: BrotesDetallePage;
  let fixture: ComponentFixture<BrotesDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrotesDetallePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrotesDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
