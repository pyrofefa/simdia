import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FenologiaBroteService } from '../services/fenologia-brote.service';

@Component({
  selector: 'app-brotes-detalle',
  templateUrl: './brotes-detalle.page.html',
  styleUrls: ['./brotes-detalle.page.scss'],
})
export class BrotesDetallePage implements OnInit {
  id = null;
  devid: any;
  brotes: any = [];
 
  constructor(private route: ActivatedRoute, public brote: FenologiaBroteService)
  {
    this.id = this.route.paramMap.subscribe(id => {
          this.devid = id.get('id');
          this.brote.getBroteId(this.devid)
            .then(res =>{
                this.brotes = res;
            }).catch(error =>{
                alert(error);
            });
      })
   }
  ngOnInit() {
  }

}
