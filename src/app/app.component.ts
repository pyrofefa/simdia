import { Component, OnChanges, OnInit } from  '@angular/core';
import { Platform, LoadingController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { FenologiasService } from './services/fenologias.service';
import { UbicacionesService } from './services/ubicaciones.service';
import { CapturasService } from './services/capturas.service';
import { FenologiaBroteService } from './services/fenologia-brote.service';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Uid } from '@ionic-native/uid/ngx';
import { Router } from '@angular/router';
import { Location } from "@angular/common";


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent{
  
  loading: any;
  toast: any;
  imei:string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public loadingController: LoadingController,
    public toastCtr: ToastController,
    public sqlite: SQLite,
    public fenologias: FenologiasService,
    public brotes: FenologiaBroteService,
    public ubicaciones: UbicacionesService,
    public capturas: CapturasService,
    public storage: Storage,
    private androidPermissions: AndroidPermissions,
    private uid: Uid,
    private router: Router,
    private location: Location

  ) {
    this.initializeApp();
    
    this.platform.backButton.subscribe(res => {
      if (this.router.url != '/home') {
        this.location.back();    
      }
      else{
        navigator['app'].exitApp();  
      }
    });
  
  }
  initializeApp() {
      this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.createDatabase();
    }); 
  }
  private createDatabase(){
    this.sqlite.create({
      name: 'main.db',
      location: 'default'
    }).then((db)=>{
        this.fenologias.setDatabase(db);
        this.brotes.setDatabase(db);
        this.ubicaciones.setDatabase(db);
        this.capturas.setDatabase(db);
        this.ubicaciones.createTable();
        this.fenologias.createTable();
        this.brotes.createTable();
        this.capturas.createTable();
    }).catch(error => {
        alert("Error! "+error);
    })
  }
  async tablas()
  {
    this.cargandoMessage('Actualizando tablas');
    //Cargando fenologias
    this.fenologias.getServiceFenologias()
      .then(res=>{
        let result: any = res;
        if(result['status'] == 'success'){
        //Cargando Fenologias Brotes
        this.brotes.getServiceFenologias()
          .then(res =>{
            let result:any = res;
            if(result['status'] == 'success'){
              setTimeout(()=>{
                this.loading.dismiss();
                this.presentToast('Tablas actualizadas correctamente');
              },1500);
            }
            else{
              setTimeout(()=>{
                this.presentToast('Ocurrió un error al actualizar las tablas. Por favor revise su conexión a internet.');
                this.loading.dismiss();
              },1500);
            }
          }).catch(error =>{
            this.loading.dismiss();
            alert("Error! "+error);
          });
        }
        else{
          setTimeout(()=>{
            this.loading.dismiss();
            this.presentToast('Ocurrió un error al actualizar las tablas. Por favor revise su conexión a internet.');
          },1500);
        }
      }).catch(error =>{
        this.loading.dismiss();
        alert("Error! "+error);
     });
  }
  async ubicacionesUpdate(){
    this.cargandoMessage('Actualizando ubicaciones');
    this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.READ_PHONE_STATE
    ).then(res =>{
        if(res.hasPermission){
          this.getUbicaciones(this.uid.IMEI);
        }
    }).catch(error =>{
        alert("Error!"+error);
    });
  }
  salir(){
    navigator['app'].exitApp();  
  }
  async cargandoMessage(text:string){
    this.loading = await this.loadingController.create({
      message: text
    });
    return this.loading.present();
  }
  async presentToast(text:string){
    this.toast = await this.toastCtr.create({
        message: text,
        duration: 2000
      });
      return this.toast.present();
  }
  getUbicaciones(imei:string){
    this.ubicaciones.getServicesTrampas(imei)
    .then(res=>{
      let result:any = res;
      if(result['status'] == 'success')
      {
       
        setTimeout(()=>{
          this.loading.dismiss();
          this.presentToast(result['message']);
          if(this.router.url == '/home'){
            this.router.navigate(['/home-update']);
          }
        },1500);
        
      }
      else
      {
        setTimeout(()=>{
          this.loading.dismiss();
          this.presentToast(result['message']);
        },1500);
      }
    }).catch(error=>{
      setTimeout(()=>{
        this.loading.dismiss();
        this.presentToast('Ocurrió un error al actualizar las ubicaciones. Por favor revise su conexión a internet.'+ error);
      },1500);
    });
  }
}