import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { UbicacionesService } from "../services/ubicaciones.service";
import { Router } from '@angular/router';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  latitud:number;
  longitud:number;
  presicion:number;
  ubicaciones:string;
  trampas:any = [];
  results:any = [];
  
  loading: any;
  imei: any;

  total:number;
  toast:any;

  constructor(  
        private geolocation: Geolocation,
        private launchNavigator: LaunchNavigator,
        public alertController: AlertController,  
        private route:Router, 
        private loadingController: LoadingController,
        private barcodeScanner: BarcodeScanner,
        public ubicacion: UbicacionesService,
        public storage: Storage,
        public toastCtr: ToastController
         ) {
    
  }
  ngOnInit(){
      this.location();
  }
  getUbicaciones(){
    this.cargandoMessage('Cargando ubicaciones cercanas');
    
    this.ubicacion.getTrampas()
    .then(res =>{
        setTimeout(()=>{
            this.trampas = res;
            this.ubicaciones = this.trampas;
            this.loading.dismiss();
          },1500);

    }).catch(error =>{
        alert(error);
        this.loading.dismiss();
    })
  }
  scan()
  {
      this.barcodeScanner.scan().then(barcodeData => {
        if(!barcodeData.cancelled){
          let str = barcodeData.text;
          let n = str.indexOf("&");
          let parseId = str.substring(0,n);
          this.ubicacion.getTrampaId(parseId)
            .then(res =>{
              if(res == null){
                  let remplace = str.split('&');//remplazando 
                  //colocando en array
                  this.results = {
                    trampa_id: remplace[0],
                    name: remplace[2],
                    y: remplace[4],
                    x: remplace[5],
                    campo: remplace[6]
                  }
                  this.ubicacion.postTrampasScan(this.results).then(res =>{
                    if(res == true){
                      this.route.navigate(['/ubicacion-detalle', remplace[0] ]);
                    }
                  }).catch(erro =>{
                    this.presentToast('Ocurrio un error intente de nuevo');
                  });
              }
              else{
                this.route.navigate(['/ubicacion-detalle', parseId ]);
              }
          }).catch(error =>{
              this.presentToast('Ocurrio un error intente de nuevo');
          })
        }
        else{
          this.route.navigate(['/home']);
        }
      }).catch(err => {
          console.log('Error', err);
      });
  }
  ubicacionDetalle(item)
  {
    this.route.navigate(['/ubicacion-detalle', item]);
  }
  /*Mensaje*/
  async message(error){
    const alert = await this.alertController.create({
      header: 'Error',
      message: error,
      buttons: ['OK']
    });
    this.loading.dismiss();
    await alert.present();
  }
  async presentToast(text:string){
    this.toast = await this.toastCtr.create({
        message: text,
        duration: 2000
      });
      return this.toast.present();
  }
  location()
  {
      this.cargandoMessage('Obteniendo mi ubicación');
      this.geolocation.getCurrentPosition()
      .then((resp) => {
          setTimeout(()=>{
              this.loading.dismiss();
              this.latitud = resp.coords.latitude;
              this.longitud = resp.coords.longitude;
              this.presicion = resp.coords.accuracy;
              this.getUbicaciones();
          },1500);
      }).catch((error) => {
        this.message(error);
        this.loading.dismiss();
      }); 
      
      let watch = this.geolocation.watchPosition();
      watch.subscribe((resp) => {
          this.latitud = resp.coords.latitude;
          this.longitud = resp.coords.longitude;
          this.presicion = resp.coords.accuracy;
      });
  }
 
  async cargandoMessage(text:string){
    this.loading = await this.loadingController.create({
      message: text
    });
    return this.loading.present();
  }
  async navigate(x:any, y:any){
    let destination = [y, x];
    let options: LaunchNavigatorOptions ={
      start: [this.latitud, this.longitud],
      app: this.launchNavigator.APP.GOOGLE_MAPS,
    }
    this.launchNavigator.navigate(destination,options)
        .then(res =>{
            alert('Success');
        }).catch(error =>{
            alert("Error! "+error);
        })
  }
}