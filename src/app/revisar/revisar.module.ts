import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RevisarPage } from './revisar.page';

const routes: Routes = [
  {
    path:'',
    redirectTo: 'registros'
  },
  {
    path: '',
    component: RevisarPage,
    children:[
      {
        path:'registros',
        loadChildren: '../registros/registros.module#RegistrosPageModule'
      },
      {
        path: 'fenologias',
        loadChildren: '../fenologias/fenologias.module#FenologiasPageModule'
      },
      {
        path: 'brotes',
        loadChildren: '../brotes/brotes.module#BrotesPageModule'
      },
      {
        path: 'ubicaciones',
        loadChildren: '../ubicaciones/ubicaciones.module#UbicacionesPageModule'
      },
      {
        path:'registro-detalle/:id',
        loadChildren: '../registro-detalle/registro-detalle.module#RegistroDetallePageModule'
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RevisarPage]
})
export class RevisarPageModule {}
