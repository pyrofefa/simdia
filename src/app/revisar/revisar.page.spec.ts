import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisarPage } from './revisar.page';

describe('RevisarPage', () => {
  let component: RevisarPage;
  let fixture: ComponentFixture<RevisarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
