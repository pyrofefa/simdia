import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'tablas', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'subir', loadChildren: './subir/subir.module#SubirPageModule' },
  { path: 'revisar', loadChildren: './revisar/revisar.module#RevisarPageModule' },
  { path: 'tablas', loadChildren: './tablas/tablas.module#TablasPageModule' },
  { path: 'ubicaciones', loadChildren: './ubicaciones/ubicaciones.module#UbicacionesPageModule' },
  { path: 'ubicacion-detalle/:id', loadChildren: './ubicacion-detalle/ubicacion-detalle.module#UbicacionDetallePageModule' },
  { path: 'fenologias', loadChildren: './fenologias/fenologias.module#FenologiasPageModule' },
  { path: 'registros', loadChildren: './registros/registros.module#RegistrosPageModule' },
  { path: 'registro-detalle/:id', loadChildren: './registro-detalle/registro-detalle.module#RegistroDetallePageModule' },
  { path: 'brotes', loadChildren: './brotes/brotes.module#BrotesPageModule' },
  { path: 'brotes-detalle/:id', loadChildren: './brotes-detalle/brotes-detalle.module#BrotesDetallePageModule' },
  { path: 'home-update', loadChildren: './home-update/home-update.module#HomeUpdatePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
