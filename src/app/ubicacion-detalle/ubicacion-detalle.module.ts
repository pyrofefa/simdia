import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UbicacionDetallePage } from './ubicacion-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: UbicacionDetallePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UbicacionDetallePage]
})
export class UbicacionDetallePageModule {}
