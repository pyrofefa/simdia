import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { UbicacionesService } from '../services/ubicaciones.service';
import { FenologiasService } from '../services/fenologias.service';
import { FenologiaBroteService } from '../services/fenologia-brote.service';
import { CapturasService } from '../services/capturas.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import * as moment from "moment"; 
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-ubicacion-detalle',
  templateUrl: './ubicacion-detalle.page.html',
  styleUrls: ['./ubicacion-detalle.page.scss'],
})
export class UbicacionDetallePage implements OnInit
{
  fecha = moment().format('YYYY-MM-DD')
  fechaHora = moment().format('YYYY-MM-DD h:mm:ss');
  ano = moment().format('YYYY');
  semana = moment(this.fecha).week();
  imei:string;
  
  latitud:number;
  longitud:number;
  presicion:number;
  id = null;
  myDate: String = new Date().toISOString();
  /*datos */
  campo: any = [];
  fenologias: any = [];
  brotes:any = [];
  captura:any = [];

  today = Date.now();
  devid: any;

  loading: any;
  toast: any;

  y:number;
  x:number;
  distancia_qr:number;
  id_bd_cel:number;

  constructor(
    private geolocation: Geolocation,
    private loadingController: LoadingController,
    private route: ActivatedRoute, 
    private back: Router,
    public toastCtr: ToastController,
    public ubicacion: UbicacionesService,
    public fenologia: FenologiasService,
    public brote: FenologiaBroteService,
    public capturas: CapturasService,
    public storage: Storage
    ) 
  {
    this.cargandoMessage('Cargando');
    this.id = this.route.paramMap.subscribe(id => {
          this.devid = id.get('id');
          this.ubicacion.getTrampaId(this.devid)
          .then(res => {
              if(res == null){
                  setTimeout(()=>{
                      this.loading.dismiss();
                      this.back.navigate(['/home']);
                      this.presentToast('No se encontró la ubicación intente nuevamente.');
                  },1500);
              }
              else{
                  this.campo = res;
                  this.location();
                  this.getFenologias();
                  this.getBrotes();
                
                  this.storage.get('imei').then(res =>{
                      this.imei = res;
                  }); 
                  //this.imei = '352570062843681'; 

                  this.captura = { 
                    captura: null, 
                    fenologia: null,
                    trampa_id: this.devid,
                    instalada: null,
                    noa: null,
                    non: null,
                    nof: null,
                    sua: null,
                    sun: null,
                    suf: null,
                    esa: null,
                    esn: null,
                    esf: null,
                    oea: null,
                    oen: null,
                    oef: null
                };
                this.calculateDistance();
                this.idBdCel();

            }
          }).catch(error =>{
            this.loading.dismiss();
            alert(error);
        })
    });
  }
  ngOnInit(){

  }
  getFenologias(){
    this.fenologias;
    this.fenologia.getFenologias()
    .then(res =>{
        this.fenologias = res;
    }).catch(error =>{
        alert(error);
    });

  }
  getBrotes(){
    this.brotes;
    this.brote.getBrotes()
    .then(res =>{
        this.brotes = res;
    }).catch(error =>{
        alert(error);
    })
  }
  save()
  {
    this.cargandoMessage('Guardando');
    if(this.presicion > 16)
    {
        setTimeout(()=>{
            this.loading.dismiss();
            this.presentToast('La precisión debe de ser menor a 16 para poder guardar el registro');
        },1500);
    }
    else if(this.captura.captura == null)
    {
      
        setTimeout(()=>{
            this.loading.dismiss();
            this.presentToast('Inserte captura');
        },1500);
    }
    else if(this.captura.fenologia == null || this.captura.nof == null || this.captura.suf == null || this.captura.esf == null || this.captura.oef == null)
    {
        setTimeout(()=>{
            this.loading.dismiss();
            this.presentToast('Inserte fenología');
        },1500);
    }
    else
    {
      let output = '';
      this.capturas.addCaptura(this.captura, this.imei, this.longitud, this.latitud, this.presicion, this.fecha, this.fechaHora, this.ano, this.semana, this.distancia_qr, this.id_bd_cel)
      .then(res=>{
        let result:any = res;
            setTimeout(()=>{
                this.loading.dismiss();
                this.back.navigate(['/home']);
              
                if(result['status'] == 'success')
                {
                    this.presentToast('Registro guardado localmente y en línea');
                }
                else{
                    this.presentToast('Registro guardado localmente');
                }
            },1500);
        }).catch(error =>{
            for(let i in error){
              output += i + ': ' + error[i]+'; ';
            }
            alert(output);
        });
    }    
  }
  location()
  {
      this.geolocation.getCurrentPosition()
      .then((resp) => {
        setTimeout(()=>{
              this.latitud = resp.coords.latitude;
              this.longitud = resp.coords.longitude;
              this.presicion = resp.coords.accuracy;
              this.loading.dismiss();
          },1500);
        }).catch((error) => {
      });  
      
      let watch = this.geolocation.watchPosition({ maximumAge: 3000, timeout: 7000, enableHighAccuracy: true });
      watch.subscribe((resp) => {
          this.latitud = resp.coords.latitude;
          this.longitud = resp.coords.longitude;
          this.presicion = resp.coords.accuracy;
      });
  }
  async cargandoMessage(text:string){
    this.loading = await this.loadingController.create({
      message: text
    });
    return this.loading.present();
  }
  async presentToast(text:string){
      this.toast = await this.toastCtr.create({
          message: text,
          duration: 2000
        });
        return this.toast.present();
  }
  calculateDistance()
  {
    this.ubicacion.calculateDistance(this.captura.trampa_id)
      .then(res =>{
          for(let result of res){
            this.y = result.y;
            this.x = result.x;

            let total:number = 0;
            let p = 0.017453292519943295;
            let c = Math.cos;
            let a = 0.5 - c((this.latitud-this.y) * p) / 2 + c(this.y * p) *c((this.latitud) * p) * (1 - c(((this.longitud- this.x) * p))) / 2;
            let dis = (12742 * Math.asin(Math.sqrt(a)));
            let mt = dis * 1000;
            this.distancia_qr = Math.trunc(mt);
            return this.distancia_qr;
          }
      }).catch(error =>{
      
      });
  }
  idBdCel(){
    this.capturas.sqliteSequence()
      .then(res=>{
        for(let result of res){
          this.id_bd_cel = result.seq;
          this.id_bd_cel= this.id_bd_cel + 1;
          return this.id_bd_cel;

        }
      })
  }
  
}