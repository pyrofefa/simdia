import { Component, OnInit } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { CapturasService } from '../services/capturas.service';
import { Router } from '@angular/router';
import * as moment from "moment"; 
import { ToastController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-registros',
  templateUrl: './registros.page.html',
  styleUrls: ['./registros.page.scss'],
})
export class RegistrosPage implements OnInit {
  searchTerm: string = '';
  capturas: any = [];
  loading: any;
  toast: any;

  inicio:any;
  fin:any;

  constructor(public sqlite: SQLite, public captura: CapturasService, private route:Router, public toastCtr: ToastController, private loadingController: LoadingController 
    ) { 
    this.getCapturas();
  }
  ngOnInit() {
  }
  getCapturas(){
    this.captura.getCapturas()
    .then(res => {
      this.capturas = res;
    }).catch(error =>{
        alert(error);
    });
  }
  registroDetails(item){
    this.route.navigate(['/registro-detalle', item]);
  }
  generate(inicio,  fin)
  {
    let fechaInicio = moment(inicio).format('YYYY-MM-DD');
    let fechaFinal = moment(fin).format('YYYY-MM-DD');
    if(typeof inicio =='undefined'){
        setTimeout(()=>{
          this.presentToast('Inserte fecha de inicio.');
        },1500);
    }
    else if(typeof fin == 'undefined'){
      setTimeout(()=>{
        this.presentToast('Inserte fecha de fin.');
      },1500);
    }
    else if(fechaInicio > fechaFinal){
        setTimeout(()=>{
          this.presentToast('La fecha de inicio no puede ser mayor que la de fin.');
        },1500);
    }
    else{
        this.cargandoMessage('Buscando');
        this.captura.getCapturasFecha(fechaInicio, fechaFinal)
          .then(res =>{
            if(res == null)
            {
              setTimeout(()=>{
                  this.loading.dismiss();
                  this.presentToast('No se encontraron capturas en las fechas seleccionadas.');
              },1500);
            }
            else{
              setTimeout(()=>{
                  this.loading.dismiss();
                  this.capturas = res;
                },1500);
              
            }
          }).catch(error =>{
            alert(error);
          })
    }
  }
  async cargandoMessage(text:string){
    this.loading = await this.loadingController.create({
      message: text
    });
    return this.loading.present();
  }
  async presentToast(text:string){
    this.toast = await this.toastCtr.create({
        message: text,
        duration: 2000
      });
      return this.toast.present();
  }
}
