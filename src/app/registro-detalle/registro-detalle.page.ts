import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CapturasService } from '../services/capturas.service';

@Component({
  selector: 'app-registro-detalle',
  templateUrl: './registro-detalle.page.html',
  styleUrls: ['./registro-detalle.page.scss'],
})
export class RegistroDetallePage implements OnInit {
  id = null;
  devid: any;
  capturas: any = [];
  fenologiaBote:string;
  
  constructor(private route: ActivatedRoute, public captura: CapturasService){
    this.id = this.route.paramMap.subscribe(id => {
      this.devid = id.get('id'); 
          this.captura.getCapturaId(this.devid)
              .then(res =>{
                  this.capturas = res;
              }).catch(error =>{
                  alert(error);
              });
    })
   }

  ngOnInit() {
  }

}
