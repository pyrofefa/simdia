import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RegistroDetallePage } from './registro-detalle.page';
import { FenologiasBrotesPipe } from '../pipes/fenologias-brotes.pipe';
import { from } from 'rxjs';


const routes: Routes = [
  {
    path: '',
    component: RegistroDetallePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegistroDetallePage, FenologiasBrotesPipe]
})
export class RegistroDetallePageModule {}
