import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, Router } from '@angular/router';
import { IonicModule, IonicRouteStrategy, ToastController, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { Uid } from '@ionic-native/uid/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Network } from '@ionic-native/network/ngx';
import { FenologiasService } from './services/fenologias.service';
import { UbicacionesService } from './services/ubicaciones.service';
import { CapturasService } from './services/capturas.service';
import { FenologiaBroteService } from './services/fenologia-brote.service';
import { HttpClientModule } from '@angular/common/http';
import { Config } from 'ionic-angular';
import { SqliteDbCopy } from '@ionic-native/sqlite-db-copy/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Geolocation, 
    LaunchNavigator, 
    Uid, AndroidPermissions, Network,
    SQLite, 
    BarcodeScanner, 
    FenologiasService,
    FenologiaBroteService, 
    UbicacionesService, 
    CapturasService, 
    Config,
    SqliteDbCopy,
    FileTransfer, FileTransferObject, File
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  loading: any;
  toast: any;
      constructor(public config: Config, 
        private uid: Uid, 
        private androidPermissions: AndroidPermissions,
        public storage: Storage, 
        public loadingController: LoadingController, 
        public toastCtr: ToastController, 
        public ubicaciones: UbicacionesService,
        private route:Router)
      {
          /**Creando Variables de configuracion con el paquete ionic-angular */
          //Publica
          config.set('smHost', 'http://187.188.195.108/siafeson/public');
          //Local
          //config.set('smHost', 'http://192.168.1.110/siafeson/public');
          config.set('dbCopy', '/data/data/siafeson.movil.simdia_2/databases/');
          config.set('crFile', 'file:////data/user/0/siafeson.movil.simdia_2/files/');
          this.storage.get('first')
            .then(val=>{
                if(val == null){
                    this.route.navigate(['/tablas']);
                }
                else{
                  this.route.navigate(['/home']);

                }
            })
         
      }
}