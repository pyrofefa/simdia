import { Component, OnInit } from '@angular/core';
import { ActionSheetController, LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { CapturasService } from '../services/capturas.service';
import { Network } from '@ionic-native/network/ngx';
import { SqliteDbCopy } from '@ionic-native/sqlite-db-copy/ngx';
import * as moment from "moment"; 
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Config } from 'ionic-angular';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-subir',
  templateUrl: './subir.page.html',
  styleUrls: ['./subir.page.scss'],
})
export class SubirPage implements OnInit {
  subir:any;
  loading:any;
  toast:any;
  ubicaciones:string;
  archivo:any = [];
  fecha = moment().unix();
  fileTransfer: FileTransferObject = this.transfer.create();

  constructor(public actionSheetController: ActionSheetController, 
              private route:Router, 
              public capturas: CapturasService,
              public loadingController: LoadingController,
              public toastCtr: ToastController,
              public network: Network,
              private sqliteDbCopy: SqliteDbCopy,
              private transfer: FileTransfer,
              public config: Config,
              public file: File
              ) { }

  ngOnInit() {
    this.capturaCount();
  }
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Revisar',
        handler: () => {
          this.route.navigate(['/revisar']);
        }
      }, {
        text: 'Subir Archivos',
        handler: () => {
          this.createFile();
        }
      }]
    });
    await actionSheet.present();
  }  
  async capturaCount(){
    this.capturas.getCapturasSubir()
        .then(res =>{
            this.subir = res;
            this.ubicaciones = this.subir.length;
        }).catch(error =>{
          alert(error)
        })
  }
  btnSubir()
  {
    this.network.onDisconnect()
    .subscribe(()=>{
        this.presentToast('No hay conexión a internet, favor de revisar la configuración');
    });
      this.cargandoMessage('Subiendo registros');
      this.capturas.updateCapturas()
          .then(res =>{
              let result:any = res;
              this.ubicaciones = '0';
              setTimeout(()=>{
                  this.loading.dismiss();
                  //this.route.navigate(['/home']);
                  if(result['status'] == 'success'){
                    this.presentToast('Registros Subidos correctamente');
                  }
                  else{
                    this.presentToast('Ocurrió un error al subir los datos');
                  }
              },1500);
          }).catch(error=>{
            alert("Error! "+error);
          })
  }
  async cargandoMessage(text:string){
    this.loading = await this.loadingController.create({
      message: text
    });
    return this.loading.present();
  }
  async presentToast(text:string){
    this.toast = await this.toastCtr.create({
        message: text,
        duration: 2000
      });
      return this.toast.present();
  }
  sendFile(){
    this.cargandoMessage('Subiendo archivo');
    const url = this.config.get('smHost') +'/api/archivo';
    const db = this.file.dataDirectory+'SIMDIA.json';

    let file = 'db_SIMDIA_'+this.fecha+'.json';
    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: file,
      headers: {}
    }
    this.fileTransfer.upload(db, url, options)
      .then(data => {
        let result: any = data;
          if(result['status']=='success')
          {
            setTimeout(()=>{
                this.loading.dismiss();
                this.presentToast('Archivo Subido correctamente');
            },1500);
          }
          else{
            setTimeout(()=>{
              this.loading.dismiss();
              this.presentToast('Ocurrió un error al subir los datos');
          },1500);
        }
      }).catch(error => {
        this.loading.dismiss();
        this.presentToast('Ocurrió un error al subir los datos');
        alert("Error! "+error);
      })
  }
  createFile(){
     
     /*Con este sube copia de BD */
     /*const db = this.config.get('dbCopy');
     this.sqliteDbCopy.copy('SIMDIA.db', 0)
      .then(res =>{
        this.sendFile();
      }).catch(error =>{
        const db = this.config.get('dbCopy');
        this.sqliteDbCopy.copyDbToStorage('SIMDIA.db',0,db,true)
          .then(res =>{
            this.sendFile();
          }).catch(error =>{
            let output = ''; 
            for(let i in error){
              output += i + ': ' + error[i]+'; ';
            }
            alert(output);
          });
      });*/

      /*Con este sube archivo json */
      this.capturas.getCapturas()
        .then(res =>{
          this.archivo = res;
          this.file.writeFile(this.file.dataDirectory,'SIMDIA.json',this.archivo,{ replace: true })
            .then(res=>{
                this.sendFile();
            }).catch(error =>{
              alert("Error! "+error);
            });
        }).catch(error =>{
          alert("Error! "+error);
        })
  } 

}
