import { Component, OnInit } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { FenologiaBroteService } from '../services/fenologia-brote.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-brotes',
  templateUrl: './brotes.page.html',
  styleUrls: ['./brotes.page.scss'],
})
export class BrotesPage implements OnInit {
  fenologias: any = [];

  constructor(private route:Router, public sqlite: SQLite, public fenologia: FenologiaBroteService) { 
    this.getFenologias();
  }

  ngOnInit() {
  }
  getFenologias(){
    this.fenologia.getBrotes()
    .then(res =>{
        this.fenologias = res;
    }).catch(error =>{
        alert(error);
    });

  }
  
  brotesDetalles(item){
    this.route.navigate(['/brotes-detalle', item]);
  }
}
