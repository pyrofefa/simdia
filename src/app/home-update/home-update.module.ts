import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeUpdatePage } from './home-update.page';
import { CalculateDistanceUpdatePipe } from '../pipes/calculate-distance-update.pipe';



const routes: Routes = [
  {
    path: '',
    component: HomeUpdatePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)

  ],
  declarations: [HomeUpdatePage, CalculateDistanceUpdatePipe],
})
export class HomeUpdatePageModule {}
