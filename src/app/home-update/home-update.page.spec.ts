import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeUpdatePage } from './home-update.page';

describe('HomeUpdatePage', () => {
  let component: HomeUpdatePage;
  let fixture: ComponentFixture<HomeUpdatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeUpdatePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeUpdatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
