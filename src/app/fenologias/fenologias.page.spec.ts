import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FenologiasPage } from './fenologias.page';

describe('FenologiasPage', () => {
  let component: FenologiasPage;
  let fixture: ComponentFixture<FenologiasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FenologiasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FenologiasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
