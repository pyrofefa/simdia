import { Component, OnInit } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite/ngx';

import { FenologiasService } from '../services/fenologias.service';


@Component({
  selector: 'app-fenologias',
  templateUrl: './fenologias.page.html',
  styleUrls: ['./fenologias.page.scss'],
})
export class FenologiasPage implements OnInit {
  
  fenologias: any = [];

  constructor(public sqlite: SQLite, public fenologia: FenologiasService){ 
    this.getFenologias();
  }

  ngOnInit() {
  }
  getFenologias(){
    this.fenologia.getFenologias()
    .then(res =>{
        this.fenologias = res;
    }).catch(error =>{
        alert(error);
    });

  }

}
