import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablasPage } from './tablas.page';

describe('TablasPage', () => {
  let component: TablasPage;
  let fixture: ComponentFixture<TablasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
