import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { UbicacionesService } from '../services/ubicaciones.service';
import { ToastController, LoadingController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Uid } from '@ionic-native/uid/ngx';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-tablas',
  templateUrl: './tablas.page.html',
  styleUrls: ['./tablas.page.scss'],
})
export class TablasPage implements OnInit {
  loading: any;
  toast: any;
  valor:any;
  dato:any;

  constructor(public route: Router, private uid: Uid, 
    private androidPermissions: AndroidPermissions,
    public storage: Storage, 
    public loadingController: LoadingController, 
    public toastCtr: ToastController, 
    public ubicaciones: UbicacionesService)
   {
      
   }
  ngOnInit() {
    this.storage.get('first').then(res =>{
      if(res == null){
        this.checkPermission();
      }
    
    });
  }
  ngOnDestroy() {
    //alert('Destroy tabla');
  }
  checkPermission()
  {
          this.androidPermissions.requestPermissions([
            this.androidPermissions.PERMISSION.READ_PHONE_STATE,
            this.androidPermissions.PERMISSION.CAMERA, 
            this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
          ]).then(res =>{
                this.androidPermissions.checkPermission(
                this.androidPermissions.PERMISSION.READ_PHONE_STATE
              ).then(res => {
                if(res.hasPermission){
                    if(this.uid.IMEI == null){
                       location.reload();
                    }
                    else{
                      this.dato = 0.5;
                      this.valor = "Guardando Permisos...";
                      this.storage.get('first').then(res =>{
                          this.cargandoMessage('Actualizando tablas');
                          if(res == null){
                            this.dato = 0.5;
                            this.valor = "Guardando Tabla...";
                            this.ubicaciones.getServicesTrampasFirst(this.uid.IMEI)
                              .then(res =>{
                                let result:any = res;
                                if(result['status'] == 'success')
                                {
                                    setTimeout(()=>{
                                      this.loading.dismiss();
                                      this.presentToast(result['message']);
                                      this.storage.set('first',1);
                                      this.storage.set('imei',this.uid.IMEI);
                                      this.route.navigate(['/home']);
                                    },1500);
                                }
                                else{
                                  setTimeout(()=>{
                                    this.loading.dismiss();
                                    this.presentToast(result['message']);
                                    this.route.navigate(['/home']);
                                  },1500);
                                }
                              }).catch(error =>{
                                alert("Error! "+error);
                              })
                          }
                      }).catch(error =>{
                        alert("Error! "+error);
                      });
                    }
                }
              }).catch(error => {
                alert("Error! "+error);
              });
          }).catch(error =>{
            alert("Error! "+error);
          });
    }
      async cargandoMessage(text:string){
        this.loading = await this.loadingController.create({
          message: text
        });
        return this.loading.present();
      }
      async presentToast(text:string){
        this.toast = await this.toastCtr.create({
            message: text,
            duration: 2000
          });
          return this.toast.present();
      }

}
