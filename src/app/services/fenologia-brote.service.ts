import { Injectable } from '@angular/core';
import { SQLiteObject  } from '@ionic-native/sqlite'
import { HttpClient } from '@angular/common/http';
import { Config } from 'ionic-angular';

@Injectable({
  providedIn: 'root'
})
export class FenologiaBroteService {
  db: SQLiteObject = null;
  fenologiaBrote: any = [];
  
  /*Para TEST:
  fenologiaBroteEstatica: any = [
    {
      id: 239,
      name: 'YEMA V1',
      image: 'v1.jpg',
      tamano: 'Yema Hinchada',
      tiempo: 0,
      escala: 'V1'
    },
    {
      id: 243,
      name: 'C. BROTES V2',
      image: 'v2.jpg',
      tamano: '1',
      tiempo: 7,
      escala: 'V2'
    },
    {
      id: 244,
      name: 'C. BROTES V3',
      image: 'v3.jpg',
      tamano: '6',
      tiempo: 12,
      escala: 'V3'
    },
    {
      id: 245,
      name: 'C. BROTES V4',
      image: 'v4.jpg',
      tamano: '12',
      tiempo: 26,
      escala: 'V2'
    },
    {
      id: 247,
      name: 'C. BROTES V5',
      image: 'v5.jpg',
      tamano: '16',
      tiempo: 34,
      escala: 'V5'
    },
    {
      id: 248,
      name: 'C. BROTES V6',
      image: 'v6.jpg',
      tamano: '20',
      tiempo: 45,
      escala: 'V6'
    },
    {
      id: 249,
      name: 'BROTE RECIO V7',
      image: 'v7.jpg',
      tamano: '>20',
      tiempo: 60,
      escala: 'V7'
    }
  ]*/
  
  constructor(public http: HttpClient, public config: Config) {
    console.log('entrando a services/fenologias-brote');
  }
  setDatabase(db: SQLiteObject)
  {
      if(this.db == null)
      {
        this.db = db;
      }
  }
  createTable()
  {
      let sql = "CREATE TABLE IF NOT EXISTS fenologia_brote (id INTERGER PRIMARY KEY, name TEXT, orden INT, id_sicafi INT, status INT, image TEXT, tamaño TEXT, tiempo INTERGER, escala TEXT)";
      return this.db.executeSql(sql,[]);
  }
  getBrotes(){
    let sql = "SELECT * FROM fenologia_brote WHERE status = 1 ORDER BY orden ASC";
    return this.db.executeSql(sql,[])
      .then(res=>{
          let brotes = [];
          for(let i=0; i < res.rows.length; i++){
              brotes.push(res.rows.item(i));
          }
          return Promise.resolve(brotes);

      }).catch(error=>{
          Promise.reject(error);
      })
  }
  getBroteId(brote:string){
      let sql = "SELECT * FROM fenologia_brote WHERE id = ?";
      return this.db.executeSql(sql,[brote])
            .then(res =>{
                let brote = [];
                for(let i=0; i < res.rows.length; i++){
                  brote.push(res.rows.item(i));
                }
                if(brote.length == 0){
                  return Promise.resolve(null);
                }
                else{
                  return Promise.resolve(brote);
                }
            }).catch(error =>{
                Promise.reject(error);
            })
  }
  
  /*Web Service*/
  getServiceFenologias()
  {
      let erase = "DELETE FROM fenologia_brote";
      this.db.executeSql(erase,[]).then(res =>{
      
      }).catch(error=>{
      
      });
      
      const url = this.config.get('smHost') +'/api/fenologia/brote';
      const params = {};

      return new Promise(resolve =>{ 
            this.http.get(url, params)
              .subscribe((data)=>{
                if(data['status'] == 'success'){
                  this.fenologiaBrote = data['data'];
                  for(let i = 0; i < this.fenologiaBrote.length; i++)
                  {
                      let sql = "INSERT INTO fenologia_brote( id, name, orden, id_sicafi, status ) VALUES ( ?, ?, ?, ?, ? )";
                      this.db.executeSql(sql,[ this.fenologiaBrote[i].id, this.fenologiaBrote[i].name, this.fenologiaBrote[i].orden, this.fenologiaBrote[i].id_sicafi, this.fenologiaBrote[i].status ]);
                  }
                  resolve(data);
                }
                else{
                  resolve(data);
                }
              },(error)=>{
                  resolve(error);
              });
      });      

    }
}
