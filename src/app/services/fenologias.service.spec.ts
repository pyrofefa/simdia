import { TestBed } from '@angular/core/testing';

import { FenologiasService } from './fenologias.service';

describe('FenologiasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FenologiasService = TestBed.get(FenologiasService);
    expect(service).toBeTruthy();
  });
});
