import { Injectable } from '@angular/core';
import { SQLiteObject  } from '@ionic-native/sqlite';
import { HttpClient } from '@angular/common/http';
import { Config } from 'ionic-angular';


@Injectable({
  providedIn: 'root'
})
export class CapturasService {
  db: SQLiteObject = null;
  r:string;
  x:number;
  y:number;
  captura:any = [];
  
  constructor(public http: HttpClient, public config: Config) { 
      console.log('Entrando a services/capturas');
  }
  setDatabase(db: SQLiteObject)
  {
      if(this.db == null)
      {
        this.db = db;
      }
  }
  createTable(){
    let sql = "CREATE TABLE IF NOT EXISTS capturas (id INTEGER PRIMARY KEY AUTOINCREMENT, imei TEXT, fecha TEXT, fechaHora TEXT, longitud REAL, latitud REAL, accuracy REAL, distancia_qr REAL, updated TEXT, status INTERGER, trampa_id INTERGER, captura INTERGER, fenologia_id INTERGER, instalada INTERGER, noa INTERGER, non INTERGER, nof INTERGER, sua INTERGER, sun INTERGER, suf INTERGER, esa INTERGER, esn INTERGER, esf INTERGER, oea INTERGER, oen INTERGER, oef INTERGER, ano TEXT, semana TEXT)";
    return this.db.executeSql(sql,[]);
  }
  getCapturas(){
    let sql = "SELECT * FROM capturas";
    return this.db.executeSql(sql,[])
    .then(res => {
        let capturas = [];
        for(let i = 0; i < res.rows.length; i++){
          capturas.push(res.rows.item(i));
        }
        return Promise.resolve(capturas);
    }).catch(error => {
        Promise.reject(error);
    });
  }
  getCapturaId(id:string){
    let sql = "SELECT fenologias.name as fenologia, trampas.name as trampa, capturas.longitud, capturas.latitud, capturas.accuracy, capturas.instalada, capturas.captura, capturas.longitud, capturas.fechaHora, capturas.fecha, trampas.y, trampas.x, trampas.campo, capturas.noa, capturas.non, capturas.nof, capturas.sua, capturas.sun, capturas.suf, capturas.esa, capturas.esn, capturas.esf, capturas.oea, capturas.oen, capturas.oef FROM trampas INNER JOIN capturas ON capturas.trampa_id = trampas.id INNER JOIN fenologias ON capturas.fenologia_id = fenologias.id WHERE capturas.id = ?";
    return this.db.executeSql(sql, [id])
          .then(res => {
              let capturas = [];
              for(let i = 0; i< res.rows.length; i++){
                capturas.push(res.rows.item(i));
              }
              if(capturas.length == 0){
                  return Promise.resolve(null);
              }
              else{
                  return Promise.resolve(capturas);
              }
          }).catch(error =>{
            Promise.reject(error);
          });
  }
  getCapturasSubir(){
    let sql = "SELECT * FROM capturas WHERE status = 2";
    return this.db.executeSql(sql,[])
    .then(res => {
        let capturas = [];
        for(let i = 0; i < res.rows.length; i++){
          capturas.push(res.rows.item(i));
        }
        return Promise.resolve(capturas);
    }).catch(error => {
        Promise.reject(error);
    });
  }
  getCapturasFecha(inicio:any, fin:any){
    let sql = "SELECT * FROM capturas WHERE fecha BETWEEN ? AND ?";
    return this.db.executeSql(sql,[ inicio, fin ])
    .then(res => {
        let capturas = [];
        for(let i = 0; i < res.rows.length; i++){
          capturas.push(res.rows.item(i));
        }
        if(capturas.length == 0){
          return Promise.resolve(null);
        }
        else{
            return Promise.resolve(capturas);
        }
    }).catch(error => {
        Promise.reject(error);
    });
  }
  addCaptura(captura:any, imei:string, longitud:any, latitud:any, presicion:any, fecha:any, fechaHora:any, ano:any, semana:any, distancia_qr:number, id_bd_cel:number)
  {
      //Noa
      if(captura.noa == true){
        captura.noa = 1;
      }
      else{
        captura.noa = 0;
      }
      //Non
      if(captura.non == true){
        captura.non = 1;
      }
      else{
        captura.non = 0;
      }
      //Sua
      if(captura.sua == true){
        captura.sua = 1;
      }
      else{
        captura.sua = 0;
      }
      //Sun
      if(captura.sun == true){
        captura.sun = 1;
      }
      else{
        captura.sun = 0;
      }
      //Esa
      if(captura.esa == true){
        captura.esa = 1;
      }
      else{
        captura.esa = 0;
      }
      //Esn
      if(captura.esn == true){
        captura.esn = 1;
      }
      else{
        captura.esn = 0;
      }
      //Oea
      if(captura.oea == true){
        captura.oea = 1;
      }
      else{
        captura.oea = 0;
      }
      //Oen
      if(captura.oen == true){
        captura.oen = 1;
      }
      else{
        captura.oen = 0;
      }
      //Instalada
      if(captura.instalada == true){
        captura.instalada = 1;
      }
      else{
        captura.instalada = 0;
      }

      let posicion = latitud + "," + longitud;
 
      const url =  this.config.get('smHost') +'/api/captura';
      const params:any = {
          fecha : fecha,
          ano : ano,
          semana : semana,
          trampa_id : captura.trampa_id,
          posicion : posicion,
          accuracy : presicion,
          fenologia_trampa_id : captura.fenologia,
          captura : captura.captura,
          imei : imei, //imei, //String  '352570062843681'
          noa : captura.noa,
          non : captura.non,
          nof : captura.nof,
          sua : captura.sua,
          sun : captura.sun,
          suf : captura.suf,
          esa : captura.esa,
          esn : captura.esn,
          esf : captura.esf,
          oea : captura.oea,
          oen : captura.oen,
          oef : captura.oef,
          status : 1,
          id_bd_cel : id_bd_cel,
          fecha_cel : fechaHora,
          method : 1,
          instalada : captura.instalada,
          distancia_qr : distancia_qr
      }
      
      return new Promise(resolve =>{
          this.http.post(url,params)
            .subscribe((data) =>{
                if(data['status'] == 'success')
                {
                  let sql = "INSERT INTO capturas (captura, imei, longitud, latitud, accuracy, fecha, fechaHora, trampa_id, status, fenologia_id, noa, non, nof, sua, sun, suf, esa, esn, esf, oea, oen, oef, instalada, ano, semana, distancia_qr ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                  this.db.executeSql(sql,[ captura.captura, imei, longitud, latitud, presicion, fecha, fechaHora, captura.trampa_id, 1, captura.fenologia, captura.noa, captura.non, captura.nof, captura.sua, captura.sun, captura.suf, captura.esa, captura.esn, captura.esf, captura.oea, captura.oen, captura.oef, captura.instalada, ano, semana, distancia_qr ]);
                  resolve(data);
                }
                else{

                  let sql = "INSERT INTO capturas (captura, imei, longitud, latitud, accuracy, fecha, fechaHora, trampa_id, status, fenologia_id, noa, non, nof, sua, sun, suf, esa, esn, esf, oea, oen, oef, instalada, ano, semana, distancia_qr ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                  this.db.executeSql(sql,[ captura.captura, imei, longitud, latitud, presicion, fecha, fechaHora, captura.trampa_id, 2, captura.fenologia, captura.noa, captura.non, captura.nof, captura.sua, captura.sun, captura.suf, captura.esa, captura.esn, captura.esf, captura.oea, captura.oen, captura.oef, captura.instalada, ano, semana, distancia_qr ]);
                  resolve(data)
                }
              },(error) =>{
                  let sql = "INSERT INTO capturas (captura, imei, longitud, latitud, accuracy, fecha, fechaHora, trampa_id, status, fenologia_id, noa, non, nof, sua, sun, suf, esa, esn, esf, oea, oen, oef, instalada, ano, semana, distancia_qr ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                  this.db.executeSql(sql,[ captura.captura, imei, longitud, latitud, presicion, fecha, fechaHora, captura.trampa_id, 2, captura.fenologia, captura.noa, captura.non, captura.nof, captura.sua, captura.sun, captura.suf, captura.esa, captura.esn, captura.esf, captura.oea, captura.oen, captura.oef, captura.instalada, ano, semana, distancia_qr ]);
                  resolve(error);
              })
      });
  
   }
   updateCapturas()
   {
        const url = this.config.get('smHost') +'/api/captura';
        //Seleccionando Capturas iguales a 2
        let sql = "SELECT * FROM capturas WHERE status = 2";
        return new Promise(resolve => {
            this.db.executeSql(sql,[])
            .then(res => {
                let capturas = [];
                for(let i = 0; i < res.rows.length; i++)
                {
                  //let id_bd_cel:string = this.sqliteSequence();
                  capturas.push(res.rows.item(i));
                  const params:any = {
                        fecha:  capturas[i].fecha,
                        ano: capturas[i].ano,
                        semana: capturas[i].semana,
                        trampa_id: capturas[i].trampa_id,
                        posicion: capturas[i].latitud +','+ capturas[i].longitud,
                        accuracy: capturas[i].accuracy,
                        fenologia_trampa_id: capturas[i].fenologia_id,
                        captura : capturas[i].captura,
                        imei: capturas[i].imei,
                        noa : capturas[i].noa,
                        non : capturas[i].non,
                        nof : capturas[i].nof,
                        sua : capturas[i].sua,
                        sun : capturas[i].sun,
                        suf : capturas[i].suf,
                        esa : capturas[i].esa,
                        esn : capturas[i].esn,
                        esf : capturas[i].esf,
                        oea : capturas[i].oea,
                        oen : capturas[i].oen,
                        oef : capturas[i].oef,
                        status : 1,
                        id_bd_cel : capturas[i].id,
                        fecha_cel : capturas[i].fechaHora,
                        method : 1,
                        instalada : capturas[i].instalada,
                        distancia_qr : capturas[i].distancia_qr
                  }
                  
                  this.http.post(url, params)
                    .subscribe((data)=>{
                      if(data['status'] == 'success')
                      {
                          let sql = "UPDATE capturas SET status = 1 where  id = ?";
                          this.db.executeSql(sql,[ capturas[i].id ] );
                          resolve(data);
                      }
                      else{
                        resolve(data);
                      }
                    },(error)=>{
                        resolve(error);
                    })
              }
            }).catch(error => {
                return Promise.reject(error);
            });
        });
   }
   sqliteSequence(){
      let sql = "SELECT MAX(seq) as seq FROM sqlite_sequence WHERE name='capturas'";
        return this.db.executeSql(sql,[]).then(res =>{
            let sentence = [];
            for(let i = 0; i < res.rows.length; i++)
            {
              sentence.push(res.rows.item(i));
            }
            if(sentence.length == 0){
              return Promise.resolve(null);
            }
            else{
              return Promise.resolve(sentence);
            }          
          }).catch(error =>{
            Promise.reject(error);
          });
        
   }
   
}
