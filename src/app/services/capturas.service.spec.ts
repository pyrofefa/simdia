import { TestBed } from '@angular/core/testing';

import { CapturasService } from './capturas.service';

describe('CapturasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CapturasService = TestBed.get(CapturasService);
    expect(service).toBeTruthy();
  });
});
