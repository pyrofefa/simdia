import { TestBed } from '@angular/core/testing';

import { FenologiaBroteService } from './fenologia-brote.service';

describe('FenologiaBroteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FenologiaBroteService = TestBed.get(FenologiaBroteService);
    expect(service).toBeTruthy();
  });
});
