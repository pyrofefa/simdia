import { Injectable } from '@angular/core';
import { SQLiteObject  } from '@ionic-native/sqlite';
import { from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Config } from 'ionic-angular';


@Injectable({
  providedIn: 'root'
})
export class UbicacionesService {
  db: SQLiteObject = null;
   x:number;
  y:number;
  fenologias: any = [];
  fenologiaBrote: any = [];
  ubicaciones: any = [];
  /*ubicacionesEstaticas: any = [
    {
      id: 259288,
      name: '048_THUE05-20-334-1877T001',
      status: 1,
      y: 29.078351,
      x: -110.945920,
      campo: 'CHARCO DE PLATA A'
    },
    {
      id: 269268,
      name: '048_THUE05-20-334-1878T001',
      status: 1,
      y: 29.078189,
      x: -110.934237,
      campo: 'EL COROZO A'
    },
    {
      id: 269289,
      name: '048_THUE05-20-334-1879T002',
      status: 1,
      y: 29.076903,
      x: -110.925916,
      campo: 'CHARCO REDONDO A'
    },
    {
      id: 208670,
      name: '105_PHUE05-26-030-0003T01',
      y: 29.080380,
      x: -110.941992,
      campo: 'Campo Prueba'
    },
    {
      id: 208672,
      name: '105_PHUE05-26-030-0003T03',
      y: 30.621354,
      x: -110.973502,
      campo: 'Casa'
    }
  ];*/

  constructor(public http: HttpClient, public config: Config) { 
    console.log('entrando a services/ubicaciones');
  }
  setDatabase(db: SQLiteObject)
  {
      if(this.db == null)
      {
        this.db = db;
      }
  }
  createTable()
  {
    let sql = "CREATE TABLE IF NOT EXISTS trampas (id INTERGER PRIMARY KEY, name TEXT, status INT, y REAL, x REAL, campo TEXT)";
    return this.db.executeSql(sql,[]);
  }
  getTrampas()
  {
    let sql = "SELECT * FROM trampas";
    return this.db.executeSql(sql,[])
    .then(res =>{
        let trampas = [];
        for(let i = 0; i < res.rows.length; i++){
            trampas.push(res.rows.item(i));
        }
        return Promise.resolve(trampas);
    }).catch(error =>{
        Promise.reject(error);
    })
  }
  getTrampaId(trampa: string){
      let sql = "Select * from trampas where id = ?";
      return this.db.executeSql(sql,[trampa])
      .then(res =>{
          let trampa = [];
          for (let i =0; i < res.rows.length; i++){
            trampa.push(res.rows.item(i));
          }
          if(trampa.length == 0){
            return Promise.resolve(null);
          }
          else{
            return Promise.resolve(trampa);
          }  
      })
      .catch(error =>{
        Promise.reject(error);
      });
  }
  postTrampasScan(capturas:any){
    let sql = "INSERT INTO trampas (id, name, status, y, x, campo) values(?, ?, ?, ?, ?, ?)";
    return this.db.executeSql(sql, [ capturas.trampa_id, capturas.name, 3, capturas.y, capturas.x, capturas.name ]).then(res =>{
      return Promise.resolve(true);
    }).catch(error =>{
      Promise.reject(error);
    });
  }
  calculateDistance(id)
  {
    let sql = "SELECT x, y FROM trampas where id = ?";
    return this.db.executeSql(sql,[id])
    .then(res =>{
        let trampa = [];
        for (let i =0; i < res.rows.length; i++){
          trampa.push(res.rows.item(i));
        }
        if(trampa.length == 0){
          return Promise.resolve(null);
        }
        else{
          return Promise.resolve(trampa);
        }  
    })
    .catch(error =>{
      Promise.reject(error);
    });
  }
  /*Web Service*/
  getServicesTrampas(imei:string)
  {
      let erase = "DELETE FROM trampas WHERE status = 1";
      this.db.executeSql(erase,[]).then(res =>{
      
      }).catch(error=>{
      
      });

      const url = this.config.get('smHost') +'/api/trampas/'+imei;
      const params = {};
            
      return new Promise(resolve => {
        this.http.get(url, params).toPromise()
          .then(data =>{
            if(data['status'] == 'success')
            {
              this.ubicaciones = data['data'];
              for(let i = 0; i < this.ubicaciones.length; i++)
              {
                  let sql = "INSERT INTO trampas (id, name, status, y, x, campo) values(?, ?, ?, ?, ?, ?)";
                  this.db.executeSql(sql, [this.ubicaciones[i].id, this.ubicaciones[i].name, this.ubicaciones[i].status, this.ubicaciones[i].latitud, this.ubicaciones[i].longitud, this.ubicaciones[i].campo ]);
              }
              resolve(data);
            }
            else{
              resolve(data);
            }

          }).catch(error =>{
              resolve(error);
          });
      });
      
  }
  getServicesTrampasFirst(imei:string)
  {
      const url = this.config.get('smHost') +'/api/trampas/primera/'+imei;
      const params = {};
      return new Promise(resolve => {
        this.http.get(url, params).toPromise()
          .then(data =>{
            if(data['status'] == 'success')
            {
              this.ubicaciones = data['trampas'];
              for(let i = 0; i < this.ubicaciones.length; i++)
              {
                  let sql = "INSERT INTO trampas (id, name, status, y, x, campo) values(?, ?, ?, ?, ?, ?)";
                  this.db.executeSql(sql, [this.ubicaciones[i].id, this.ubicaciones[i].name, this.ubicaciones[i].status, this.ubicaciones[i].latitud, this.ubicaciones[i].longitud, this.ubicaciones[i].campo ]);
              }
              this.fenologias = data['arbol'];
              for(let i = 0; i < this.fenologias.length; i++)
              {
                let sql = "INSERT INTO fenologias( id, name, orden, id_sicafi, status ) VALUES ( ?, ?, ?, ?, ? )";
                this.db.executeSql(sql,[ this.fenologias[i].id, this.fenologias[i].name, this.fenologias[i].orden, this.fenologias[i].id_sicafi, this.fenologias[i].status ]);
              }
              this.fenologiaBrote = data['brote'];
              for(let i = 0; i < this.fenologiaBrote.length; i++)
              {
                let sql = "INSERT INTO fenologia_brote( id, name, orden, id_sicafi, status ) VALUES ( ?, ?, ?, ?, ? )";
                this.db.executeSql(sql,[ this.fenologiaBrote[i].id, this.fenologiaBrote[i].name, this.fenologiaBrote[i].orden, this.fenologiaBrote[i].id_sicafi, this.fenologiaBrote[i].status ]);
              }
              resolve(data);
            }
            else{
              resolve(data);
            }

          }).catch(error =>{
              resolve(error);
          });
      });
      
  }
}