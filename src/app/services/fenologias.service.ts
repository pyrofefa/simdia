import { Injectable } from '@angular/core';
import { SQLiteObject  } from '@ionic-native/sqlite'
import { from } from 'rxjs';
import { forEach } from '@angular/router/src/utils/collection';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from 'ionic-angular';

@Injectable({
  providedIn: 'root'
})
export class FenologiasService {
  db: SQLiteObject = null;
  fenologias: any = [];
  //Para TEST: fenologiasEstaticas: any = [{id: 239, name: 'BROTACIÓN' }, { id: 243, name: 'DESARROLLO (CRECIMIENTO)' }, { id: 244, name: 'DORMANCIA' }, { id: 245, name: 'FLORACIÓN' }, { id: 246, name: 'FRUCTIFICACION' }];

  constructor(public http: HttpClient, public config: Config) {
      console.log('entrando a services/fenologias');
  }
  setDatabase(db: SQLiteObject)
  {
      if(this.db == null)
      {
        this.db = db;
      }
  }
  createTable()
  {
      let sql = "CREATE TABLE IF NOT EXISTS fenologias (id INTERGER PRIMARY KEY, name TEXT, orden INT, id_sicafi INT, status INT)";
      return this.db.executeSql(sql,[]);
  }
  getFenologias()
  {
    let sql = "SELECT * FROM fenologias WHERE status = 1 ORDER BY orden ASC";
    return this.db.executeSql(sql,[])
    .then(res =>{
        let fenologias = [];
        for(let i = 0; i < res.rows.length; i++){
            fenologias.push(res.rows.item(i));
        }
        return Promise.resolve(fenologias);
    }).catch(error =>{
        Promise.reject(error);
    })
  }
  /*Web Service*/
  getServiceFenologias()
  {
    let erase = "DELETE FROM fenologias";
    this.db.executeSql(erase,[]).then(res =>{
    
    }).catch(error=>{
    
    });
    
    const url = this.config.get('smHost') +'/api/fenologia/arbol';
    const params = {};
    const headers = new HttpHeaders({ 
        'Content-Type': 'application/json', 
        'X-Requested-With': 'XMLHttpRequest',
        'MyClientCert': '',
        'MyToken': ''
      });
    return new Promise(resolve =>{
        this.http.get(url,{ headers:headers, params })
            .subscribe((data)=>{
              if(data['status'] == 'success')
              {
                    this.fenologias = data['data'];
                    for(let i = 0; i < this.fenologias.length; i++)
                    {
                      let sql = "INSERT INTO fenologias( id, name, orden, id_sicafi, status ) VALUES ( ?, ?, ?, ?, ? )";
                      this.db.executeSql(sql,[ this.fenologias[i].id, this.fenologias[i].name, this.fenologias[i].orden, this.fenologias[i].id_sicafi, this.fenologias[i].status ]);
                    }
                    resolve(data);
              }
              else{
                  resolve(data);
              }
            },(error)=>{
                resolve(error);
            });
        });
   }
}
